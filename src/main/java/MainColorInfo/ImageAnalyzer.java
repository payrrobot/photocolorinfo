package MainColorInfo;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ImageAnalyzer {
    private static Logger log = Logger.getLogger(ImageAnalyzer.class.getName());
    public static float DELTAE = 2.3f; //Коэффициент цветового различия


    public static void analyze(String inputImage, String outputImage) {
        File sourceFile = new File(inputImage);
        BufferedImage source = null;
        try {
            source = ImageIO.read(sourceFile);
        } catch (IOException e) {
            log.log(Level.WARNING, e.toString());
        }
        int newWidth = (int) (source.getWidth() + source.getWidth() * 0.3 + 1);
        BufferedImage result = new BufferedImage(newWidth, source.getHeight(), source.getType());

        ArrayList<ColorCounter> colorList = ImageAnalyzer.pixelsAnalyze(source, result);

        //Определение самого большого каунтера в ArrayList
        Collections.sort(colorList);
        Color firstColor = colorList.get(0).getColor();
        Color secondColor = colorList.get(1).getColor();

        //Заполнение дополнительного пространства основными цветами
        for (int x = source.getWidth() + 1; x < newWidth; x++) {
            for (int y = 0; y < source.getHeight() / 2; y++) {
                result.setRGB(x, y, firstColor.getRGB());
            }
            for (int y = source.getHeight() / 2 + 2; y < source.getHeight(); y++) {
                result.setRGB(x, y, secondColor.getRGB());
            }
        }
        File output = new File(outputImage);
        try {
            ImageIO.write(result, "jpg", output);
        } catch (IOException e) {
            log.log(Level.WARNING, e.toString());
        }

    }
    //По пиксельный анализ изображения
    private static ArrayList<ColorCounter> pixelsAnalyze(BufferedImage source, BufferedImage result) {
        ArrayList<ColorCounter> colorList = new ArrayList<>();

        for (int y = 0; y < source.getHeight(); y++) {
            log.log(Level.FINE, "Строка №" + y + "/" + source.getHeight());
            log.log(Level.FINE, "Найдено цветов: " + colorList.size());
            for (int x = 0; x < source.getWidth(); x++) {

                Color nowColor = new Color(source.getRGB(x, y));

                result.setRGB(x, y, nowColor.getRGB());

                if (colorList.isEmpty()) {
                    colorList.add(new ColorCounter(nowColor));
                }

                for (int i = 0; i < colorList.size(); i++) {

                    LABColor tabLAB = colorList.get(i).getLabColor();
                    LABColor nowLAB = ColorConverter.RGBtoLAB(nowColor);

                    float deltaE = ColorConverter.deltaE(nowLAB, tabLAB);

                    if (deltaE <= DELTAE) {
                        colorList.get(i).incrementCounter();
                        break;
                    }
                    if (i == colorList.size() - 1 && deltaE > DELTAE) {
                        colorList.add(new ColorCounter(nowColor));
                        break;
                    }
                }
            }
        }
        return colorList;
    }
}
