package MainColorInfo;

import java.awt.Color;

public class ColorConverter {
    public static float[] CIE10_D65 = {94.811f, 100f, 107.304f}; //Daylight CIE10
    public static float[] CIE2_D65 = {95.047f, 100f, 108.883f}; //Daylight CIE2

    public static float[] RGBtoXYZ(Color color){
        float[] rgb = {color.getRed(), color.getGreen(), color.getBlue()};
        float[] xyz = new float[3];

        for (int i = 0; i < rgb.length; i++){
            rgb[i] /= 255f;
            if ( rgb[i] > 0.04045)
                rgb[i] = (float)Math.pow(( ( rgb[i] + 0.055f ) / 1.055f ), 2.4f);
            else
                rgb[i] /= 12.92f;
            rgb[i] *= 100;
        }

        float x = 0.412453f * rgb[0] + 0.35758f * rgb[1] + 0.180423f * rgb[2];
        float y = 0.212671f * rgb[0] + 0.71516f * rgb[1] + 0.072169f * rgb[2];
        float z = 0.019334f * rgb[0] + 0.119193f * rgb[1] + 0.950227f * rgb[2];

        xyz[0] = x;
        xyz[1] = y;
        xyz[2] = z;

        return xyz;
    }

    public static LABColor XYZtoLAB(float x, float y, float z, float[] tristimulus){
        float[] xyz = {x, y, z};
        LABColor labColor = new LABColor();

        for (int i = 0; i < xyz.length; i++){
            xyz[i] /= tristimulus[i];
            if (xyz[i] > 0.008856)
                xyz[i] = (float)Math.pow(xyz[i], 0.33f);
            else
                xyz[i] = (7.787f * xyz[i]) + ( 0.1379310344827586f );
        }

        labColor.setL(( 116f * xyz[1] ) - 16f);
        labColor.setA(500f * (xyz[0] - xyz[1] ));
        labColor.setB(200f * (xyz[1] - xyz[2] ));

        return labColor;
    }

    public static LABColor RGBtoLAB(Color color){
        float[] tristimulus = CIE10_D65;

        float[] xyz = RGBtoXYZ(color);
        LABColor labColor = XYZtoLAB(xyz[0], xyz[1], xyz[2], tristimulus);

        return labColor;
    }

    public static float deltaE (LABColor labNow, LABColor labInColorCounter){
        float e = (float) Math.sqrt(Math.pow(labNow.getL() - labInColorCounter.getL(), 2) + Math.pow(labNow.getA() - labInColorCounter.getA(), 2) + Math.pow(labNow.getB() - labInColorCounter.getB(), 2));

        return e;
    }
}
