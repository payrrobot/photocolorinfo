package MainColorInfo;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {

        String inputImage = "source.jpg";
        String outputImage = "output.jpg";
        ImageAnalyzer.analyze(inputImage, outputImage);

    }
}
