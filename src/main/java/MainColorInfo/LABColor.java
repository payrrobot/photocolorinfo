package MainColorInfo;

public class LABColor {

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LABColor other = (LABColor) obj;
        return this.getL() == other.getL() && this.getA() == other.getA() && this.getB() == other.getB();
    }

    private float l, a, b;

    LABColor(float newl, float newa, float newb){
        l = newl;
        a = newa;
        b = newb;
    }

    LABColor(){ // Default 252, 15, 192 RGB color такой цвет встречается очень редко
        l = 57.27f;
        a = 86.36f;
        b = -29.53f;
    }

    public float getL(){
        return l;
    }

    public void setL(float l){
        this.l = l;
    }

    public float getA(){
        return a;
    }

    public void setA(float a){
        this.a = a;
    }

    public float getB(){
        return b;
    }

    public void setB(float b){
        this.b = b;
    }

}
