package MainColorInfo;

import java.awt.Color;

public class ColorCounter implements Comparable<ColorCounter> {
    private Color color;
    private int counter;
    private LABColor labColor;

    ColorCounter(Color newColor){
        color = newColor;
        counter = 1;
        labColor = ColorConverter.RGBtoLAB(newColor);
    }

    public Color getColor(){
        return color;
    }

    public void setColor(Color color){
        this.color = color;
    }

    public int getCounter(){
        return counter;
    }

    public void setCounter(int counter){
        this.counter = counter;
    }

    public void incrementCounter(){
         counter++;
    }

    public LABColor getLabColor(){
        return labColor;
    }

    @Override
    public int compareTo(ColorCounter o) {
        return o.getCounter() - this.getCounter();
    }
}
