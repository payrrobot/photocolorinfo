package MainColorInfo;

import org.junit.Assert;
import org.junit.Test;

import java.awt.Color;

import static org.junit.Assert.*;

public class ColorConverterTest {

    @Test
    public void RGBtoLAB() {
        Color actualRGB = new Color(0, 255, 255);
        LABColor actual = ColorConverter.RGBtoLAB(actualRGB);

        LABColor expected = new LABColor(91.19871f, -47.33327f, -14.95173f);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void deltaE() {
        Color red = new Color(255, 0, 0);
        Color cyan = new Color(0, 255, 255);

        Float deltaE = ColorConverter.deltaE(ColorConverter.RGBtoLAB(red), ColorConverter.RGBtoLAB(cyan));

        Assert.assertTrue(deltaE > ImageAnalyzer.DELTAE);
    }
}