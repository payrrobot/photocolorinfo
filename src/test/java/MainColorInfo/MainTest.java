package MainColorInfo;

import com.google.common.hash.Hashing;
import org.junit.Assert;
import org.junit.Test;

import javax.xml.crypto.dsig.DigestMethod;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.*;

public class MainTest {
    Logger log = Logger.getLogger(MainTest.class.getName());

    @Test
    public void main() {
        String test = "src/test/resources/test.jpg";
        String actual = "src/test/resources/temp/actual.jpg";

        ImageAnalyzer.analyze(test, actual);

        DigestInputStream disActual = null;
        DigestInputStream disExpected = null;
        try {
            InputStream expected = Files.newInputStream(Paths.get("src/test/resources/testExpected.jpg"));
            InputStream actualOutput = Files.newInputStream(Paths.get("src/test/resources/temp/actual.jpg"));
            MessageDigest md = MessageDigest.getInstance("MD5");
            disExpected = new DigestInputStream(expected, md);
            disActual = new DigestInputStream(actualOutput, md);

        } catch (IOException | NoSuchAlgorithmException e) {
            log.log(Level.WARNING, e.toString());
        }

        Assert.assertEquals(disActual.toString(), disExpected.toString());
    }
}